def add(one, two)
  one + two
end

def subtract(one, two)
  one - two
end

def sum(array)
  total = 0
  array.each do |num|
    total += num
  end
  total
end

def multiply(one, two)
  one * two
end

def power(one, two)
  one ** two
end

def factorial(one, two)
  if a == 0
    0
  else
    (1..a).to_a.reduce(:*)
  end
end
