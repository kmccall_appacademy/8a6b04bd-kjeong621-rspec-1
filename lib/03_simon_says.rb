def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num = 2)
  word_array = [word]
  if num <= 2
    word_array.push(word)
    return word_array.join(' ')
  else
    (num - 1).times do
      word_array.push(word)
    end
    word_array.join(' ')
  end
end

def start_of_word(word, num)
  word.slice(0, num)
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(sentence)
  little_words = ["the", "over", "and"]
  array = sentence.split
  array.map do |word|
    word.capitalize! unless little_words.include?(word)
  end
  array[0].capitalize!
  array.join(' ')
end
