def translate(words)
  array = words.split
  array.map! do |word|
    latinize(word)
  end
  array.join(" ")
end

def latinize(word)
  vowels = "aeiou"
  qu = "qu"
  until vowels.include?(word[0].downcase)
    if word[0..1] == qu
      word = word[2..-1] + word[0..1]
    else
      word = word[1..-1] + word[0]
    end
  end
  word + "ay"
end
